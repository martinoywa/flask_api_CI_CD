import unittest
from app import sum


class Test(unittest.TestCase):

    def test_sum(self):
        self.assertEqual(sum(4, 5), 9)


if __name__ == "__main__":
    unittest.main()